package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("Hello")
	result := make(map[string]string)
	result["message"] = "Hello World v2"

	result["pod_namespace"] = os.Getenv("MY_POD_NAMESPACE")
	result["pod_node"] = os.Getenv("MY_POD_NODE")
	result["pod_name"] = os.Getenv("MY_POD_NAME")
	result["pod_ip"] = os.Getenv("MY_POD_IP")

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(result)
}

func main() {
	log.Println("Server started at 8080")
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
